/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import 'react-native-gesture-handler';
import React from 'react';
import {
  Dimensions,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  ImageBackground,
  TouchableOpacity,
  Alert,
} from 'react-native';
import SearchBar from 'react-native-search-bar';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
var t = require('tcomb-form-native');

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const Form = t.form.Form;
const User = t.struct({
  email: t.String,
  password: t.String,
});
const options = {
  fields: {
    email: {
      error: 'Wrong input!',
    },
    password: {
      error: 'Wrong input!',
      password: true,
      secureTextEntry: true,
    },
  },
};

// The homepage of the app. The Facebook login button does not work but gives an alert instead, the email login moves to the emaillogin page.
function HomeScreen({navigation}) {
  return (
    <>
      <StatusBar hidden={true} />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <ImageBackground
            source={require('./assets/intro.png')}
            style={styles.body}>
            <Image
              source={require('./assets/iconBasicSet12Camera512.png')}
              style={styles.cameraImage}
              resizeMode="cover"
            />
            <TouchableOpacity
              onPress={() => Alert.alert('Facebook Button pressed')}
              style={[styles.buttonStyle, styles.facebookButton]}>
              <View style={styles.buttonText}>
                <Text style={[styles.textRegular, styles.facebookText]}>
                  sign in with Facebook
                </Text>
                <Image
                  source={require('./assets/facebook-icon.png')}
                  style={styles.facebookImage}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate('EmailLogin')}
              style={[styles.buttonStyle, styles.emailButton]}>
              <View style={styles.buttonText}>
                <Text style={[styles.textRegular, styles.emailText]}>
                  sign in with email
                </Text>
              </View>
            </TouchableOpacity>
          </ImageBackground>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

/*function login(value) {
  fetch('https://react-native-case-api.firebaseapp.com/auth/login', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email: {value.email},
      password: {value.password},
    }),
  });
}*/

/*function handleSubmit() {
  const value = this._form.getValue();
  Alert.alert('value: ', value);
}*/

// The login page for email logins. The Submit button moves the user straight to the Movies page.
function LoginEmailScreen({navigation}) {
  return (
    <>
      <StatusBar hidden={true} />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <Form
              //ref={(c: any) => (this._form = c)}
              type={User}
              options={options}
            />
            <TouchableOpacity
              //onPress={() => handleSubmit()}
              onPress={() => navigation.navigate('Movies')}
              style={[styles.buttonStyle, styles.emailFormButton]}>
              <View style={styles.buttonText}>
                <Text style={[styles.textRegular, styles.emailFormText]}>
                  sign in >
                </Text>
              </View>
            </TouchableOpacity>
            <Text style={styles.textTitle}>
              The Submit button moves you toward the movie page. Please do try
              it out even though the login does not work!
            </Text>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

// The screen including all the movies and the deprecated search function. Clicking a movie opens the movie popup.
function MovieScreen({navigation}) {
  class MovieThumbnail extends React.Component<
    {key: string; isLeft: Boolean},
    {}
  > {
    render() {
      return (
        <View
          style={
            this.props.isLeft
              ? styles.movieThumbnailLeft
              : styles.movieThumbnailRight
          }>
          <TouchableOpacity onPress={() => navigation.navigate('Joker')}>
            <Image
              source={require('./assets/joker.png')}
              style={styles.movieImage}
            />
            <Text style={styles.movieText}>Joker</Text>
          </TouchableOpacity>
        </View>
      );
    }
  }

  const movies = [
    <MovieThumbnail key="Joker1" isLeft={true} />,
    <MovieThumbnail key="Joker2" isLeft={false} />,
    <MovieThumbnail key="Joker3" isLeft={true} />,
    <MovieThumbnail key="Joker4" isLeft={false} />,
    <MovieThumbnail key="Joker5" isLeft={true} />,
    <MovieThumbnail key="Joker6" isLeft={false} />,
    <MovieThumbnail key="Joker7" isLeft={true} />,
    <MovieThumbnail key="Joker8" isLeft={false} />,
  ];
  return (
    <>
      <StatusBar hidden={true} />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <SearchBar
              placeholder="Search"
              //style={styles.searchbar}
              //onChangeText={updateSearch(this)}
              //value={search}
            />
            <View style={styles.movieRow}>{movies}</View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

/*function updateSearch(search: string) {
  this.setState({search});
};

function showElements(key: string) {
  if (key !== "") {
    let m = [MovieThumbnail];
    for (let movie of movies) {
      if (key === movie.name) {
        m.push(movie);
      }
    }
    return <View style={styles.movieRow}>{m}</View>;
  } else {
    return <View style={styles.movieRow}>{movies}</View>;
  }
}*/

// The page with the moviedetails.
function MovieDetails({navigation}) {
  return (
    <>
      <StatusBar hidden={true} />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <TouchableOpacity onPress={() => navigation.navigate('Movies')}>
              <Text style={styles.close}>X</Text>
            </TouchableOpacity>
            <Image
              source={require('./assets/joker.png')}
              style={styles.jokerImage}
            />
            <Text style={styles.jokerTitle}>Joker</Text>
            <Text style={styles.jokerText}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </Text>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const Stack = createStackNavigator();

function App(this: any) {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="EmailLogin" component={LoginEmailScreen} />
        <Stack.Screen name="Movies" component={MovieScreen} />
        <Stack.Screen name="Joker" component={MovieDetails} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

// From here on out starts the stylesheet for the application.
const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'rgb(251, 251, 251)',
  },
  body: {
    width: DEVICE_WIDTH,
    height: DEVICE_HEIGHT,
  },
  textTitle: {
    fontFamily: 'omnessemibold',
    fontSize: 17,
    marginTop: 64,
    color: 'rgb(229, 0, 125)',
    padding: 12,
    alignSelf: 'center',
  },
  textRegular: {
    fontFamily: 'omnesregular',
    fontSize: 14,
  },

  // Button styling (overarching)
  buttonStyle: {
    alignSelf: 'center',
    backgroundColor: '#FF0000',
    borderTopLeftRadius: 22,
    borderTopRightRadius: 22,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 22,
  },
  buttonText: {
    flexDirection: 'row',
    paddingHorizontal: 28,
    paddingVertical: 16,
  },
  // Special settings for the facebook login button
  facebookButton: {
    marginTop: 113,
    backgroundColor: 'rgb(59, 89, 152)',
  },
  facebookText: {
    color: 'rgb(251, 251, 251)',
  },
  facebookImage: {
    width: 14,
    height: 14,
    marginLeft: 17,
  },

  // Special settings for the email login button
  emailButton: {
    marginTop: 25,
    marginBottom: 31,
    backgroundColor: 'rgb(251, 251, 251)',
  },
  emailText: {
    color: 'rgb(229, 0, 125)',
  },
  emailFormButton: {
    backgroundColor: 'rgb(229, 0, 125)',
  },
  emailFormText: {
    color: 'rgb(251, 251, 251)',
  },

  // Searchbar styling
  searchbar: {
    marginTop: 76,
  },

  // Miscelaneous images
  cameraImage: {
    width: 256,
    height: 256,
    marginHorizontal: 32,
    marginTop: 40,
    alignSelf: 'center',
  },

  // Settings for the way the movie thumbnails are shown
  movieRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  movieThumbnailLeft: {
    width: '49%',
    height: 158,
    backgroundColor: 'rgb(251, 251, 251)',
    marginRight: '1%',
  },
  movieThumbnailRight: {
    width: '49%',
    height: 158,
    backgroundColor: 'rgb(251, 251, 251)',
    marginLeft: '1%',
  },
  movieImage: {
    marginTop: 24,
    width: '100%',
    height: 89,
  },
  movieText: {
    fontFamily: 'omnesregular',
    color: 'rgb(0, 0, 0)',
    marginTop: 7,
    marginLeft: 8,
  },

  // The styling for the movie detail page
  close: {
    color: 'rgb(243, 0, 146)',
    fontFamily: 'omnessemibold',
    fontSize: 35,
    marginLeft: 17,
    marginTop: 32,
    marginBottom: 11,
  },
  jokerImage: {
    width: DEVICE_WIDTH,
  },
  jokerTitle: {
    alignSelf: 'center',
    marginTop: 39,
    fontFamily: 'omnessemibold',
    fontSize: 17,
    color: 'rgb(0, 0, 0)',
  },
  jokerText: {
    alignSelf: 'center',
    marginTop: 8,
    marginHorizontal: 17,
    fontFamily: 'omnesregular',
    fontSize: 17,
    color: 'rgb(161, 161, 161)',
  },
});

export default App;
