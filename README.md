# ReactTest SocialBrothers

React Native test-app for Social Brothers

# What works?

- The design of the application has been implemented as asked
- All pages are as they should be
- Movement between pages is as it should be
- It works on all tested android devices

# What almost works?

- The login function (which has an issue with gathering the desired information from the form) 
- A search functionality (which should work if it wasn't for the fact that the elements cannot find their own keys)

# What does not work?

- The facebook login does not work 
- The GET function for the movies does not work